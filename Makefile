APP=secdim.lab.csharp

all: build

test:
	docker build --target build --build-arg TESTSPEC=usability --rm .

securitytest:
	docker build --target build --build-arg TESTSPEC=security --rm .

build:
	docker build --rm --tag=$(APP) .

run:
	docker run -it --rm $(APP)

clean:
	docker image rm $(APP)
	docker system prune
	docker image prune -f

.PHONY: all test securitytest clean

